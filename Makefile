#! / Bin / bash
MANAGE = python3 manage.py

all: clean env install activate prepare run

env: 
     python3 -m venv env

activate:
	. env/bin/activate

install:
	pip3 install -r requirements.txt

collect:
	$(MANAGE) collectstatic

database:
	$(MANAGE) makemigrations
	$(MANAGE) migrate

clean:
	find . -name "*.pyc" -delete

run: manage.py
	$(MANAGE) runserver

prepare: collect database
