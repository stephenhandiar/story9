from django.test import TestCase, Client
from django.urls import resolve,reverse
from .views import index, home


# Create your tests here.
class UnitTest(TestCase):
     def test_url_exists(self):
          response = self.client.get('/')
          self.assertEqual(response.status_code, 200)

     def test_page_uses_index_template(self):
          response = Client().get('/')
          self.assertTemplateUsed(response, 'awal.html')

     def test_landing_use_correct_views(self):
          target = resolve('/')
          self.assertEqual(target.func, index)

     def test_url_exists_login(self):
          response = self.client.get('/home/')
          self.assertEqual(response.status_code, 302)

     # def test_page_uses_index_template_login(self):
     #      response = Client().get('/home/')
     #      self.assertTemplateUsed(response, 'login.html')

     def test_landing_use_correct_views_login(self):
          target = resolve('/home/')
          self.assertEqual(target.func, home)