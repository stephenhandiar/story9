from django.shortcuts import render, redirect

# Create your views here.

def index(request):
     if request.user.is_authenticated:
          return redirect('home/')
     else:
          return render(request, 'awal.html')

def home(request):
     num_visits = request.session.get('num_visits', 0)
     request.session['num_visits'] = num_visits + 1

     context = {'num_visits': num_visits,
    }     
     if not request.user.is_authenticated:
          return redirect('login')
     else:
          return render(request, 'home.html', context=context)
